#!/bin/sh


#
# in rasp-config enable i2c and spi
# in /boot/config.txt
#
#interrupt is pin number GPIOxx
#oscillator is mcp2515 crystal value in MHz
#
#   SPI CE0 add: 
#dtoverlay=mcp2515-can0,oscillator=8000000,interrupt=24
#dtoverlay=spi-bcm2835-overlay
#
#   SPI CE1 add: 
#dtoverlay=mcp2515-can1,oscillator=8000000,interrupt=24
#dtoverlay=spi-bcm2835-overlay
# 
# reboot
#

sudo ip link set can0 up type can bitrate 500000

echo "example send frame"
echo " => cansend can0 00000001#4341"
echo " => cansend can0 001#4341"

echo "example receive frame"
echo " => candump can0"

