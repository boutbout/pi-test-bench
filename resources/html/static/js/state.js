
var wsState = null;

$(document).ready(function() {
    if (!window.console)        window.console = {};
    if (!window.console.log)    window.console.log = function() {};


    wsState = new WsState();
    wsState.start();


});

var cfgchange = function()
{
    var cfgname = document.getElementById("cfg_list").value;
    wsState.set('config','cfgname',cfgname);
}



class WsState {

    constructor(){
        this.url = "ws://" + location.host + "/state";
        this.socket = null;
        this.listner = [];
    }

    start() {
        this.socket = new WebSocket(this.url);
        this.socket.onmessage   = this.onSocketMsg.bind(this);
        this.socket.onclose     = this.onSocketClose.bind(this);
    }

    onSocketMsg(event)
    {
        var msg = JSON.parse(event.data);
        console.log('Received:',msg);
        if ( msg.error )
        {
            alert('Error :'+msg.error);
        }
        switch( msg.sect )
        {
            case 'config':
                switch (msg.key)
                {
                    case 'cfgname':
                        var e = document.getElementById("cfg_list");
                        e.value = msg.value;
                        wsState.update('config','dashboard');
                        wsState.update('files','list');
                        break;
                    case 'dashboard':
                        var e = React.createElement(DashboardItemGroup,
                                                    msg.value
                                                   );
                        ReactDOM.render(e,
                                        document.getElementById('dashboard')
                                       );
                        break;
                }
                break;
            case 'test':
                switch (msg.key)
                {
                }
                break;
            case 'files':
                switch (msg.key)
                {
                    case 'list':
                        var e = React.createElement(FileList,
                                                    {files:msg.value}
                                                   );
                        ReactDOM.render(e,
                                        document.getElementById('files')
                                       );
                        break;
                    default:
                }
                break;
        }
        if ( ( this.listner[msg.sect]            ) &&
             ( this.listner[msg.sect][msg.key]   ) 
           )
        {
            this.listner[msg.sect][msg.key].forEach(function(cb){
                cb(msg.value);
            });
        }
    }

    onSocketClose(e) {
        var f = function(){
            console.log("WebSocketClient: Close retry...");
            clearTimeout(this);
            wsState.start();
        }
        setTimeout(f,5000);
    }

    send(msg)
    {
        console.log('Sending:',msg);
        this.socket.send(JSON.stringify(msg));
    }


    set(sect,key,value) {
         var msg = {
             "sect":sect,
             "key":key,
             "value":value
         }
         this.send(msg);
    }

    update(sect,key) {
         var msg = {
             "sect":sect,
             "key":key
         }
         this.send(msg);
    }

    addWatch(sect,key,callback) {
        if ( ! this.listner[sect] )
            this.listner[sect] = []
        if ( ! this.listner[sect][key] )
            this.listner[sect][key] = []
        this.listner[sect][key].push(callback);
    }

    delWatch(sect,key,callback) {
        var idx = this.listner[sect][key].indexOf(callback);
        this.listner[sect][key].splice(idx,1);
    }

};
    


