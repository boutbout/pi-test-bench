
$(document).ready(function() {
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};

    cmdshell = document.getElementsByClassName("cmdshell");
    for (i = 0; i < cmdshell.length; i++) {
        var e = cmdshell[i];
        e.onclick = function(){
            id = this.id.split(':')
            var cmd         = id[0]
            var shell_name  = id[1]
            var msg = { "cmd":cmd, "name":shell_name }
            switch ( cmd )
            {
                case "clear":
                    break;
                default:
                    break;
            }
            this.classList.remove("btn-success");
            this.classList.remove("btn-danger");
            this.classList.add("btn-default");
            shell_updater.socket.send(JSON.stringify(msg));
        };
    }
    shell_updater.start();
});

var shell_updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/shell";
        shell_updater.socket = new WebSocket(url);
        shell_updater.socket.onmessage = function(event) {
            shell_updater.showMessage(JSON.parse(event.data));
        }
    },

    showMessage: function(msg) {
        shell_updater.binding[msg.cmd](msg);
    },

};

shell_updater.binding = [];

shell_updater.binding["addLine"] = function(msg)
{
    var ansi_up = new AnsiUp;

    //var samp = document.createElement('samp');
    //samp.innerHTML = ansi_up.ansi_to_html(msg.line);

    var div  = document.createElement('div');
    //div.append(samp);
    div.innerHTML = ansi_up.ansi_to_html(msg.line);

    var shell = $("#shell_" + msg.name)[0];

    shell.append(div);

    // scrol down if near of botton
    var offsetBotton = shell.scrollHeight-(shell.scrollTop+shell.clientHeight);
    if ( offsetBotton < 2*div.clientHeight )
    {
        shell.scrollTop = div.offsetTop;
    }

    cmdshell = document.getElementsByClassName("cmdshell");
    for (i = 0; i < cmdshell.length; i++) {
        var e = cmdshell[i];
        e.classList.remove("btn-success");
        e.classList.remove("btn-danger");
        e.classList.add("btn-default");
    }
};

shell_updater.set_btn_ok = function(name)
{
    e = document.getElementById(name);
    e.classList.remove("btn-default");
    e.classList.remove("btn-danger");
    e.classList.add("btn-success");
}

shell_updater.binding["clear"] = function(msg)
{
    var shell = $("#shell_" + msg.name)[0];
    shell.innerHTML = "";
    shell_updater.set_btn_ok("clear:"+msg.name);
};

shell_updater.binding["restart"] = function(msg)
{
    shell_updater.set_btn_ok("restart:"+msg.name);
};

