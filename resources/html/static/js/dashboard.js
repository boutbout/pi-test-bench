

class DashboardItem extends React.Component {

    /* TODO: Checkbox autoSync */

    constructor(props) {
        super(props);
        this.state = {
            value   : null,
            modif   : false,
            busy    : false,
        }
        wsState.update('test',this.props.name);

        let f = function (value) {
            // Update component state whenever the data source changes
            console.log(this.props.name,'updated to ',value);
            if ( this.state.modif )
            {
                this.state.busy = false;
                this.setValue(null);
            }
            else
            {
                this.setState({value:value,busy:false});
            }
        }
        this.handleChangeCb = f.bind(this);
    };

    componentDidMount() {

        // Subscribe to changes
        wsState.addWatch('test',this.props.name,this.handleChangeCb);
    }

    componentWillUnmount() {
        if ( this.timedSend )
        {
            clearTimeout(this.timedSend);
            this.timedSend = null;
        }
        // Clean up listener
        wsState.delWatch('test',this.props.name,this.handleChangeCb);
    }

    render() {
        var className   = 'default';
        if ( this.state.busy )
        {
            className   = 'warning';
        }
        else if ( this.state.modif )
        {
            className   = 'info';
        }

        return (<div className="col-md-6 no-gutters item">
                    <div className="table-responsive">
                      <table className="table dashboard-table">
                        <tbody>
                          <tr className={"bg-"+className}>
                            <td className="item-label">
                              <label key="label">{this.getLabel()}</label>
                            </td>
                            <td className="item-value">
                                {this.renderValue()}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div>);
    }


    /**
      * Set new value and start timer to delay sending to server
      * to avoid saturation
      * @param[in] value new wanted value.
      */
    setValue(value) 
    {
        var sendDelay = this.applyDelay || 100;
        if ( this.state.busy )
        {
            console.log(this.props.name,"is busy Value");
            return
        }

        if ( value === null )
        {
            value = this.state.value;
            sendDelay = 10;
        }

        value = this.checkValue(value);

        var sendValue = function ()
        {
            console.log("Send new Value");
            if ( this.timedSend )
            {
                clearTimeout(this.timedSend);
                this.timedSend = null;
            }

            var timeout = function()
            {
                if ( this.sendTimeout )
                {
                    clearTimeout(this.sendTimeout);
                    this.sendTimeout = null;
                }
                this.setState({busy:false});
            }

            if ( this.sendTimeout )
            {
                clearTimeout(this.sendTimeout);
            }

            this.sendTimeout = setTimeout(timeout.bind(this),5000);

            this.setState({busy:true,modif:false});
            wsState.set('test',this.props.name,this.state.value);
        }

        this.setState({value:value,modif:true});

        if ( this.timedSend )
        {
            clearTimeout(this.timedSend)
        }

        this.timedSend = setTimeout(sendValue.bind(this),sendDelay);

    };

    /**
      * this function can be overridden to change default input label 
      */
    getLabel() {
        var label = this.props.label || this.props.name;
        return label+':';
    }

    /**
      * this function can be overridden to change Apply button input label 
      */
    getButtonText()
    {
        return 'Set';
    }

    /**
      * this function can be overridden to change value rending. 
      */
    renderValue()
    {
        let value = this.state.value;
        return (<input type="text" key={name} value={value} ></input>)
    }

    /**
      * This function can be overridden to change value checking. 
      * @param[in] value new wanted value.
      * @return saturated or current value, but a valid value.
      */
    checkValue(value)
    {
        return value;
    }

};


class DashboardItemBool extends DashboardItem {

    constructor(props) {
        super(props);
    };

    onToggle() {
        this.setValue(!this.state.value);
    };

    renderValue()
    {
        var name = this.props.name;
        var click = this.onToggle.bind(this);
        if ( this.state.value == true )
        {
            return (<button 
                    className="btn btn-success" key={name} onClick={click}>
                    On
                    </button>);
        }
        else
        {
            return (<button 
                    className="btn btn-danger" key={name} onClick={click}>
                    Off
                    </button>);
        }
    }

};


class DashboardItemNumber extends DashboardItem {

    constructor(props) {
        super(props);
        this.applyDelay = 1000;
    };

    checkValue(value)
    {
        var val = Number(value);
        if ( ( this.props.min !== null ) && ( val < this.props.min ) )
            val = this.props.min;
        else if ( ( this.props.max !== null ) && ( val > this.props.max ) )
            val = this.props.max;
        return val;
    }

    updateValue(e)
    {
        var val = e.target.value;
        this.setValue(val);
    }

    renderValue()
    {
        let click   = this.updateValue.bind(this);
        let min     = this.props.min;
        let max     = this.props.max;
        let step    = +this.props.step||1;

        let value   = +this.state.value;
        return (<input className="form-control" type="number" key={name} 
                onChange={click} min={min} max={max} value={value} step={step}
                ></input>)
    };

};

class DashboardItemFile extends DashboardItem {

    constructor(props) {
        super(props);
        this.state.files = [];
        this.files_cb = this.setFiles.bind(this);
    };

    setFiles(files) {
        console.log("Files:",files)
        this.setState({files:files});
    }


    componentDidMount() {
        super.componentDidMount();
        // Subscribe to changes
        wsState.addWatch('files','list',this.files_cb);
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        // Clean up listener
        wsState.delWatch('files','list',this.files_cb);
    }

    checkValue(value)
    {
        for ( let i=0 ; i < this.state.files.length; i++ )
        {
            if ( value == this.state.files[i].name )
                return value;
        }
        return this.state.value;
    }

    updateValue(e)
    {
        var val = e.target.value;
        this.setValue(val);
    }


    renderValue()
    {
        var content = [];
        if ( this.state.files )
        {
            let f = function (x,i){
                return React.createElement('option',{'key':i},x.name);
            };
            content = this.state.files.map( f );
        }

        content.unshift(React.createElement('option', { 'key':'None' }, ''));

        return React.createElement('select',
                                   {
                                   'key':'select',
                                   'className':'form-control',
                                   'value':this.state.value||'',
                                   'onChange':this.updateValue.bind(this)
                                   },
                                   content
                    );
    };

};


class DashboardItemGroup extends React.Component {

    constructor(props) {
        super(props);
    };


    render() {
        var typeName = {
            'bool'      :DashboardItemBool,
            'number'    :DashboardItemNumber,
            'enum'      :DashboardItem,
            'action'    :DashboardItem,
            'screen'    :DashboardItem,
            'group'     :DashboardItem,
            'shell'     :DashboardItem,
            'file'      :DashboardItemFile,
            'string'    :DashboardItem,
            'group'     :DashboardItemGroup
        };
        var content = [];
        var level = this.props.level || 0;

        if ( this.props.content )
        {
            let f = function (x,i){
                x.key = i;
                x.level = level+1;
                return React.createElement(typeName[x.type],x);
            }
            content = this.props.content.map(f);
        }

        var className = "group-level-"+level

        /* return React.createElement('div',{key:'div'},content); */
        return ( <div className={className}>
                   <table className="table dashboard-table">
                     <caption>{this.props.label}</caption>
                     <tbody>
                       <tr><td>{content}</td></tr>
                     </tbody>
                   </table>
                 </div>
                 );
    };

};

class FileItem extends React.Component {

    constructor(props) {
        super(props);
    };

    getName(){
        return this.props.name || "NoName";
    }

    getSize(){
        return this.props.size || 0;
    }

    getDate() {
        if ( this.props.date )
        {
            let d = new Date(this.props.date);
            return dateFormat(d, "ddd mmm dd yyyy HH:MM:ss");
        }
        return "";
    }

    onBtnDel()
    {
        if ( this.props.name )
            wsState.set('files','remove',this.props.name);
    }

    render() {
        var content = [
            React.createElement('td', {key:"name"}, this.getName()),
            React.createElement('td', {key:"size"}, this.getSize()),
            React.createElement('td', {key:"date"}, this.getDate()),
            React.createElement('td', {key:"btn" },
                React.createElement('button',
                                    { 
                                    'className':'btn btn-default',
                                    'onClick':this.onBtnDel.bind(this)
                                    },
                                    React.createElement('span',
                                                        {
                                                        'className':'glyphicon glyphicon-remove'
                                                        }
                                                        )
                                   )
            )
        ];
        return React.createElement('tr',{key:this.key},content);
    };

};

class FileList extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        var content = [];

        if ( this.props.files )
        {
            let f = function (x,i){
                x.key = i;
                return React.createElement(FileItem,x);
            }
            content = this.props.files.map(f);
        }

        let tbody = React.createElement('tbody',{key:'tbody'},content);
        var t = React.createElement('table',{key:'table',className:'table'},tbody);
        var u = React.createElement(Uploader,{key:'Uploader'});
        return React.createElement('div',null, [t,u]);
        //return React.createElement('div',null, [t]);

    };

};

class Uploader extends React.Component 
{
    constructor(props) {
        super(props);
    }

    submit(e) {
        console.log('sending file...');
        e.preventDefault();

        var f = document.getElementById('UploadFile');

        var formData = new FormData();
        formData.append('filearg', f.files[0]);

        var request = new XMLHttpRequest();
        request.open("POST",'/')
        request.send(formData)
        console.log('sending file!');
    }


    render() {
        var i = React.createElement('input',{ 
                                    'type':'file', 
                                    'name':'filearg',
                                    'key':'UploadFile',
                                    'id':'UploadFile'
                                    });
        var b = React.createElement('button',
                                   {
                                   'key':'button',
                                   'className':'btn btn-default',
                                   },
                                   'Send');
        return React.createElement('form',
                                   { 'onSubmit':this.submit.bind(this)},
                                   [i,b]
                                   );

        /*
        return React.createElement('input',{ 
                                    'type':'file', 
                                    'name':'filearg',
                                    'id':'UploadFile',
                                    'onchange':this.submit.bind(this)
                                    });
        return React.createElement('div',null,[i,b]);
        return (
              <form onSubmit={this.submit}>
                <input type="file" name="filearg" id="UploadFile"/>
                <input type="submit" value="submit" />
              </form>
              );
        return (
              <form onSubmit={this.submit}>
                <div class="input-group">
                  <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Browse&hellip; <input type="file" id='UploadFile' multiple />
                    </span>
                  </label>
                  <input type="text" class="form-control" readonly />
                </div>
              </form>
              );
         */
    }
}



/*
DashboardItem.propTypes = {
    name    : React.PropTypes.string.isRequired,
    readonly: React.PropTypes.boolean,
    type    : React.PropTypes.oneOf([
                                    'bool',
                                    'int',
                                    'enum',
                                    'action',
                                    'screen',
                                    'group',
                                    'shell',
                                    'file',
                                    'string'
                                    ]),
};
*/
