#!/bin/sh 

export GDBC_HOST=${1:-localhost}
export GDBC_PATH=${2:-arm-none-eabi-gdb}

ARGS="$ARGS --no_ptb"
ARGS="$ARGS -vvv"


./src/script.py $ARGS \
        "gdbc|config|setLogLevel|DEBUG" \
        "gdbc|config|setGdbPath|openOCD|${GDBC_PATH}" \
        "gdbc|config|setElf|flash|pnbchrono_v2-debug_firm_btldr.out" \
        "gdbc|config|setElf|symbol|pnbchrono_v2-debug_firm_btldr.out" \
        "post|gdbc|~/pnbchrono_v2-debug_firm_btldr.out" \
        "gdbc|gdbc|connection|${GDBC_HOST}|3333" \
        "gdbc|gdbc|flash" \
        "gdbc|gdbc|symbol" \
        "gdbc|gdbc|run" \

sleep 15
./src/script.py $ARGS "gdbc|gdbc|stop"

