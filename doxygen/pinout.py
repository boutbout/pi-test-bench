
"""@package Pinout

Raspberry_pi Raspberry PI
  
  AD9850 spi:/dev/spidev0.0

   1) 3V3   
   2) 5V
   3) SDA           GPIO2   =>
   4) 5V            
   5) SCL           GPIO3   =>
   6) GND           
   7)               GPIO3   => POWER 12V
   8) TX            GPIO14  =>
   9) GND           
  10) RX            GPIO15  =>
  11)               GPIO17  =>
  12) PWM           GPIO18  => RPM
  13)               GPIO27  => loop
  14) GND           
  15)               GPIO22  => break
  16)               GPIO23  =>
  17) 3V3       
  18)               GPIO24  =>  CAN_INT                     => CAN_H
  19) MOSI-ALT0     GPIO10  =>  CAN_SPI_MOSI/AD9850_D7/DATA => ZOUT2 (sine)
  20) GND
  21) MISO          GPIO9   =>  CAN_SPI_MISO                => CAN_L
  22)               GPIO25  =>  AD9850_RESET                => OUT1  (square)
  23) SPI_CLK-ALT0  GPIO11  =>  CAN_SPI_CLK/AD9850_W_CLK    => ZOUT1 (sine)
  24) SPI_CE0-ALT0  GPIO8   =>  AD9850_FQ_UD                => OUT2  (square)
  25) GND       
  26) SPI_CE1       GPIO7   =>  CAN_CS


  @subsection pnbchrono_v1 PNBChrono V1

   1) RPM   (3V3)  Green
   2) Break (12V)  White
   3) KLine (12V)  Brown
   4) VBAT  (12V)  Red
   5) SPEED (3V3)  Yellow
   6) VCC   (3V3)  Orange
   7) LOOP  (3V3)  Blue
   8) GND   ( 0V)  Black

  @subsection pnbchrono_v2 PNBChrono V2

   1) ADC_EXT1  (3V3)   Blue    (2)
   2) ADC_FRONT (3V3)   White   (2)
   3) NC(1W)    (3V3)  
   4) CAN_H     (3V3)  
   5) CAN_L     (3V3)  
   6) CAN2_H    (3V3)  
   7) CAN2_L    (3V3)  
   8) SPEED     (12V)   Yellow  (1)
   9) KLine     (12V)   Brown   (1)
  10) VBAT      (12V)   Red     (1)

  11) ADC_EXT2  (3V3)   White   (2)
  12) ADC_REAR  (3V3)   Black   (2)
  13) SCL_EXT   (3V3)   Green   (2)
  14) SDA_EXT   (3V3)   Yellow  (2)
  15) 3V3_SWI   (3V3)   Orange  (2)
  16) 5V_SWI    ( 5V)   Red     (2)
  17) RPM       (12V)   Green   (1)
  18) Break     (12V)   White   (1)
  19) LOOP      (12V)   Blue    (1)
  20) GND       ( 0V)   Black   (1)

"""
