

import os
from time import sleep
from pin import pin_class

class specs:
    gpio = { 
        'select':pin_class(11,delay_on=0.1,delay_off=0.1,pol=False),
        'usb5v' :pin_class(16,delay_on=0.1,delay_off=0.1,pol=True),
        'power' :pin_class(7,delay_on=1,delay_off=1),
        'rpm'   :pin_class(12),
        'loop'  :pin_class(13),
        'break' :pin_class(15),
    }

    cmd_line = {
        'gdbserver':['openocd','-f','./resources/openOCD/pnbchrono.cfg']
    }

    ttys_swo = {
        'baudrate'  : 1000000,
        'port'      : "/dev/ttyAMA0"
    }

    def __init__(self):
        f = os.path.join("..","out","pnbchrono_v1-debug_firm_btldr.out")
        f = os.path.abspath(f)
        self.elf =  os.environ.get('FIRM_BTLD_ELF',f)

    def reset(self):
        sleep(1)
        pass

