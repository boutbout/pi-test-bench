PROJECTNAME?=${project_name}

ARCH=PC

CFG_DIR         ?=$(CURDIR)/config
TARGET          ?=default
TARGET_DIR      ?=$(CFG_DIR)/$(TARGET)

OUT_DIR         ?=out/$(TARGET)
BUILD_DIR       ?=build/$(TARGET)

DOC_DIR         =$(CURDIR)/doc
SRC_DIR         =$(CURDIR)/src
RES_DIR         =$(CURDIR)/ressources
LIB_DIR         =$(CURDIR)/lib
TOOL_DIR        =$(CURDIR)/tools
DOXYGEN_FILE    =$(CURDIR)/doxygen/Doxyfile

OBJ_DIR         =$(BUILD_DIR)/build
DEP_DIR         =$(BUILD_DIR)/dep
PRE_DIR         =$(BUILD_DIR)/pre
LST_DIR         =$(BUILD_DIR)/lst
ELF_DIR         =$(BUILD_DIR)/elf
EXE_DIR         =$(OUT_DIR)/exe
TMP_DIR         =$(OUT_DIR)/tmp

GENERATED_FILES=$(PARAMS_GEN_JSON_FILE)

# managed by NuttX
#LINKER_SECTION  = $(PWD)/linker/$(FAMILLY).ld

# here to be first one
# Default build is debug build
#all: begin build info
all: begin

PYTHON?=/usr/bin/python

#####################################################################
## Definitions of toolchain.                                        #
## You might need to change paths/etc to match your system          #
## This makefile assumes that the tools are in the users PATH       #
#####################################################################
#
#TOOLCHAIN_MINGW_DIR?=/usr
#
##patch for eclipse uses
#WINDOWSCS = $(PATH_MINGW_TOOLCHAIN)
#
#TOOLCHAIN_BIN_DIR=$(TOOLCHAIN_MINGW_DIR)/bin/
#
##FLASHINGTOOL_DIR = "${APPDATA}/energymicro/energyAware Commander"
##FLASHINGTOOL_BIN = ${FLASHINGTOOL_DIR}/eACommander.exe
#
#GDBTOOL = $(TOOLCHAIN_BIN_DIR)/gdb
#
#
#CC 	    = $(TOOLCHAIN_BIN_DIR)/gcc
#LD 	    = $(TOOLCHAIN_BIN_DIR)/ld
#AR 	    = $(TOOLCHAIN_BIN_DIR)/ar
#OBJCOPY = $(TOOLCHAIN_BIN_DIR)/objcopy
#OBJDUMP = $(TOOLCHAIN_BIN_DIR)/objdump
#RM 	    = rm -rf
#
####################################################################
# Files Definitions                                                #
####################################################################

#
# search all file in list for listed directory.
# param 1 list of folder to search
# param 2 pattern file searched
# return list of file found with relative path.
find = $(foreach dir,$(1),$(foreach d,$(wildcard $(dir)/*),$(call find,$(d),$(2))) $(wildcard $(dir)/$(strip $(2))))

-include $(TARGET_DIR)/Makefile_target.mk
-include $(SRC_DIR)/Makefile_main.mk
include $(call find , $(SRC_DIR) $(LIB_DIR), Makefile.mk)

INCLUDEPATHS += $(CFG_DIR)
INCLUDEPATHS += $(TARGET_DIR)

INCLUDEFLAGS=$(addprefix -I,$(INCLUDEPATHS))

$(info *********************************************** )
$(info Selected Target : $(TARGET_DIR) )
$(info Project Path    : $(abspath $(PWD)))
$(info *********************************************** )

####################################################################
# Flags Definitions                                            	   #
####################################################################

# Error filter
##ERROR_FILTER=grep -E "INFOR|ERR|WARN|FATAL"

# ASM files.
#ASFLAGS += -Ttext 0x0

# C flags.
#CFLAGS += -DPROJECTNAME=$(PROJECTNAME)
#CFLAGS += -Wall
#CFLAGS += -Wundef
#CFLAGS += -ftabstop=4 # tab size for error reporting
#CFLAGS += $(addprefix -I,$(INCLUDEPATHS))

#OPTIMIZATION:= $(findstring -O0, $(CFLAGS))
#CFLAGS+= $(patsubst -O%,-DCFLAGS_O%, $(OPTIMIZATION))

## Linker Flags.
##LDFLAGS += -Map=$(LST_DIR)/$(PROJECT_BIN).map

####################################################################
# rules Definitions                                                #
####################################################################
#
#D_SRC = $(C_SRC:.c=.d)
#
##make list of source paths, sort also removes duplicates
#
#
#D_OBJS  = $(addprefix $(DEP_DIR)/, $(D_SRC:.c=.d))
#
#C_OBJS  = $(C_SRC:.c=.o)
#C_OBJS  := $(addprefix $(OBJ_DIR)/, $(C_OBJS) )
#
#S_OBJS  = $(S_SRC:.s=.o)
#S_OBJS  := $(addprefix $(OBJ_DIR)/, $(S_OBJS))
#
#C_PRE   = $(C_SRC:.c=.pre)
#C_PRE   := $(addprefix $(PRE_DIR)/, $(C_PRE) )
#
#D_PATH = $(sort $(dir $(D_OBJS)))
#C_PATH = $(sort $(dir $(C_OBJS)))
#
## include depence files
#-include $(wildcard $(DEP_DIR)/* )

.PHONY: begin
begin: $(OBJ_DIR) $(LST_DIR) $(EXE_DIR) $(DEP_DIR) $(TMP_DIR) $(PRE_DIR) $(GENERATED_FILES)

.PHONY: build
#build: $(PROJECTNAME_BIN)
build:

show_files:
	$(Q)echo " MK  :"; for file in $(MAKEFILE_LIST)    ; do echo -e "MK \t$$file" ; done
	$(Q)echo " C   :"; for file in $(PY_SRC)           ; do echo -e "C  \t$$file" ; done
	$(Q)echo " INC :"; for file in $(INCLUDEPATHS)     ; do echo -e "INC\t$$file" ; done

info: $(PROJECTNAME_BIN)
	$(Q)echo "    [INF] *****************************************************************"
	$(Q)echo "    [INF]  * Selected Target      : "$(TARGET_DIR)
	$(Q)echo "    [INF] *****************************************************************"

#pre: begin $(PRE_DIR) $(GENERATED_FILES) $(C_PRE)

.PHONY: doc_view
doc_view: doc
	$(Q)start   doc/html/index.html

# Create directories
.PHONY: doc
doc: $(PY_SRC)
	$(Q)echo "Created Doc ..."
	$(Q)doxygen $(DOXYGEN_FILE)

# Create directories
$(OBJ_DIR):
	$(Q)mkdir -p $(OBJ_DIR)
	$(Q)echo "Created build directory."

$(ELF_DIR):
	$(Q)mkdir -p $(ELF_DIR)
	$(Q)echo "Created Elf directory."

$(EXE_DIR):
	$(Q)mkdir -p $(EXE_DIR)
	$(Q)echo "Created executable directory."

$(DEP_DIR):
	$(Q)mkdir -p $(DEP_DIR)
	$(Q)echo "Created dependence directory."

$(PRE_DIR):
	$(Q)mkdir -p $(PRE_DIR)
	$(Q)echo "Created dependence directory."

$(LST_DIR):
	$(Q)mkdir -p $(LST_DIR)
	$(Q)echo "Created list directory."

$(TMP_DIR):
	$(Q)mkdir -p $(TMP_DIR)
	$(Q)echo "Created temp directory."


.PHONY:tags
tags:
	$(Q)cd $(TOPDIR)
	$(Q)ctags -R $(sort $(SRC_DIR) $(CFG_DIR) $(INCLUDEPATHS) $(C_SRC))

.PHONY: clean 
clean:
	$(Q) -rm -rf $(GENERATED_FILES)



