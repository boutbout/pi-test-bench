#!/usr/bin/python3
#-*- coding: utf-8 -*-

import logging

log = logging.getLogger()

if __name__ == "__main__":
    try:
        import colorlog

        formater = colorlog.ColoredFormatter(
            "%(log_color)s%(levelname)-8s%(asctime)s%(reset)s %(message)s",
            datefmt=None,
            reset=True,
            log_colors={
                'DEBUG':    'cyan',
                'INFO':     'green',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            },
            style='%'
        )

        handler = logging.StreamHandler()
        handler.setFormatter(formater)

        logger = colorlog.getLogger()
        logger.addHandler(handler)
    except:
        FORMAT = '[%(asctime)s] %(message)s'
        logging.basicConfig(format=FORMAT)
        log.exception('log color failed')

import argparse
import os
import sys
import tempfile
import tornado.ioloop
import tornado.web as web
import tornado.escape
import tornado.websocket
from importlib import import_module


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("It works")

    def post(self):
        fileinfo = self.request.files['filearg'][0]
        fname = fileinfo['filename']
        path = self.application.tempfile(fname)
        fh = open(path, 'w')
        fh.write(fileinfo['body'])
        log.info("Uploaded file : %s"%(path))

class GDBCHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self):
        GDBCHandler.waiters.add(self)

    def on_close(self):
        GDBCHandler.waiters.remove(self)

    @classmethod
    def send(cls, waiter, req, ret):
        try:
            msg = {'ret':ret, 'req':req}
            json = tornado.escape.json_encode(msg)
            logging.debug("sending json "+json)
            waiter.write_message(json)
        except:
            logging.error("Error sending json", exc_info=True)

    def on_message(self, msg):
        logging.info("got message %r", msg)
        ret = None
        try:
            req = tornado.escape.json_decode(msg)
            ret = self.application.cmd(**req)
        except Exception as e:
            log.exception("command failed %s"%(msg))
            ret = {'exception':str(e)}
        self.send(self, req, ret)

class Application(tornado.web.Application):
    """
        web server for GDB Client interface
    """
    def __init__(self, cfg_root, gdb_type=None, gdb_path=None):
        """ constructor """
        self.cfg_list = {}
        self.gdbc = None
        handlers = [
            (r"/", MainHandler),
            (r"/gdbc", GDBCHandler ),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            xsrf_cookies=False,
        )

        self.periodic = tornado.ioloop.PeriodicCallback(self._loop, 100)
        self.periodic.start()

        self.setGdbPath(gdb_type, gdb_path)

        super().__init__(handlers, **settings)
        log.info("GDBWebSocket Ready")

    def tempfile(self, fname):
        """ give absolute path to store @b fname.  """
        return os.path.join(tempfile.gettempdir(),
                            "ptb-gdbc-%s"%(fname)
                           )

    def _loop(self):
        if self.gdbc:
            self.gdbc.loop()

    def setGdbPath(self, _type, path):
        """config: unload last gdb client and load new one.

            _type: type of gdb client to use. List in src/gdbc/*.py
            path: gdbc executable path.
            Note: if path is None no new gdbc will be loaded.
        """
        ret = None
        if self.gdbc:
            self.gdbc.kill()
        self.gdbc = None
        if path:
            self.gdbc = import_module('gdbc.'+_type).gdbc(self, path)
            ret = _type+':'+path
        return ret

    def setLogLevel(self, lvl):
        """config: set Application log level

            0 or CRITICAL
            1 or ERROR
            2 or WARNING
            4 or INFO
            5 or DEBUG
            6 or NOTSET
        """
        l = [
        'CRITICAL',
        'ERROR',
        'WARNING',
        'INFO',
        'DEBUG',
        'NOTSET'
        ]

        
        if lvl.isdigit():
            lvl = l[int(lvl)]

        log.setLevel(lvl)
        return lvl

    def setElf(self, key, fname):
        """config: set elf filename to use
            see gdb.base class for list of possible key 
        """
        ret = "%s:%s"%(key, fname)
        if self.gdbc:
            self.gdbc.elf[key] = self.tempfile(fname)
        else:
            ret = {'error':'gdbc not selected!'}
        return ret

    def cmd(self, **kwargs):
        sect = kwargs.pop('sect', 'gdbc')
        func = kwargs.pop('func')
        args = kwargs.get('args', None)
        log.info("***** %s : %s : %s *****"%(sect, func, str(args)))
        if sect == 'config':
            ret = getattr(self, func)(*args)
        elif sect == 'gdbc':
            if self.gdbc:
                ret = getattr(self.gdbc, func)(*args)
            else:
                ret = {'error':'gdbc not selected!'}
        return ret



if __name__ == "__main__":

    topDir  = os.path.dirname(os.path.split(os.path.realpath(__file__))[0])
    cfgDir  = os.path.join(topDir, 'config')
    sys.path.append(cfgDir)

    parser = argparse.ArgumentParser(description='Pi-test bench.')

    parser.add_argument('--port', type=int, default=8889, 
                        help='listen port')
    parser.add_argument('--gdb_type', type=str, default=None, 
                        help='gdb client type')
    parser.add_argument('--gdb_path', type=str, default=None, 
                        help='gdb client binary')
    parser.add_argument('-v', '--verbose', action="count", 
                        help="verbose level... Repeat up to three times.")

    args = parser.parse_args()

    if not args.verbose:
        log.setLevel('ERROR')
    elif args.verbose == 1:
        log.setLevel('WARNING')
    elif args.verbose == 2:
        log.setLevel('INFO')
    elif args.verbose >= 3:
        log.setLevel('DEBUG')
    else:
        log.critical("UNEXPLAINED NEGATIVE COUNT!")

    app = Application(cfgDir, args.gdb_type, args.gdb_path)
    app.listen(args.port)
    try:
        tornado.ioloop.IOLoop.instance().start()
    finally:
        app.setGdbPath(None, None)

