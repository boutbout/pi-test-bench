#!/usr/bin/python3
#-*- coding: utf-8 -*-

import logging 
from time import sleep

log = logging.getLogger('simulation_gpio')

HIGH    ="high"
LOW     ="low"
OUT     ="output"
IN      ="input"
BOARD   ="board"

class _pwm(object):
    def __init__(self,pin,freq):
        self.pin  = pin
        self.freq = float(freq)
        self.duty = 50.
        self.show("Create")

    def show(self,msg):
        log.debug("%s %s"%(msg,str(self)))

    def __str__(self):
        return "PWM GPIO %d freq %f duty %f"%(self.pin,self.freq,self.freq)

    def start(self,duty):
        self.duty = float(duty)
        self.show("Start")

    def stop(self):
        self.show("Stop")
    
    def ChangeFrequency(self,freq):
        self.freq = float(freq)
        self.show("SetFreq")

def setwarnings(value):
    log.debug("Set Warning %s"%(value))

def setup(pin,direction,initial):
    log.debug("Set GPIO %d in %s"%(pin,direction))

def output(pin,value):
    log.debug("Set GPIO %d at %s"%(pin,value))

def setmode(value):
    log.debug("Set mode %s"%(value))

def PWM(pin,freq):
    return _pwm(pin,freq)

