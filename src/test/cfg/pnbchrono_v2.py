#!/usr/bin/python3
#-*- coding: utf-8 -*-

from test.base import baseTestClass
from test.elements import \
         baseGroup,     \
         pinOutputTrmin,\
         pinOutputPwm,  \
         pinOutputPulse,\
         pinOutputPP,   \
         player,        \
         fakeGPS,       \
         baseNumber,    \
         baseBool,      \
         baseKeyboard   \

from test.pin import pin_class
from test import ad9850

import subprocess
import serial
import os

class canKeyboard(baseKeyboard):

    def __init__(self, *args, **kwargs):
        self.keys_idx = {
            'Back':1,
            'Up':2,
            'Down':4,
            'Ok':8
        }
        self.state = 0
        super().__init__(*args, **kwargs)
        #TODO use python-can instead of command line
        os.system("sudo ip link set can0 up type can bitrate 500000");

    def keychanged(self,elem,old,val):
        mask = self.keys_idx[elem.name]
        if val:
            self.state |= mask;
        else:
            self.state &= ~mask;
        #TODO use python-can instead of command line
        os.system("cansend can0 00000001#%02X"%(self.state))
        


class elements(baseGroup):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        def gpsOpenOut(): 
            return serial.Serial(port="/dev/ttyAMA0", baudrate=115200)

        self.usb5v = pinOutputPP(self,'usb5v', 'USB 5v',
                                 pinCfg={
                                 'pin':16, 
                                 'delay_on':0.1, 
                                 'delay_off':0.1, 
                                 'pol':True
                                 }
                                )
        self.power = pinOutputPP(self,'power', 'Power',
                                 pinCfg={
                                 'pin':7, 
                                 'delay_on':1, 
                                 'delay_off':1
                                 }
                                )
        self.loop = pinOutputPulse(self,'loop', 'Loop',
                                   pinCfg={'pin':13}
                                  )
        self.breaking = pinOutputPP(self,'break', 'Break',
                                    pinCfg={'pin':15}
                                   )
        self.rpm = pinOutputTrmin(self,'rpm', 'Motor RPM',
                                    pinCfg={'pin':12}
                                   )
        self.keyboard = canKeyboard(self,'keyboard', 'KeyBoard',
                                     keys=['Back','Up','Down','Ok'],
                                    )
        self.gps_file = player(self,'gps_file_player','GPS File',
                          openOut=gpsOpenOut
                         )
        self.gps_sim = fakeGPS(self,'gps_sim_player','GPS sim',
                          openOut=gpsOpenOut
                         )

class obj(baseTestClass):
    def __init__(self, *args, **kwargs):

        self.pin_select = pin_class(11, delay_on=0.1, delay_off=0.1, pol=True)

        super(obj, self).__init__(*args, **kwargs)
        self.elementGroup = elements(self,'dashboard',self.cfgname)

    def start(self):
        self.pin_select.init();
        self.pin_select.on();
        super().start()

    def kill(self):
        super().kill()
        self.pin_select.deinit();


    # GDBServer process ==========================
    def gdbserver_process(self):
        """
            give a configured gdb server subprocess 
        """
        cmd_line = ['openocd','-f','./resources/openOCD/pnbchrono.cfg']
        return subprocess.Popen(cmd_line, bufsize=1, 
                                stdin =subprocess.PIPE, 
                                stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, 
                                close_fds=self.ON_POSIX
                               )

    # opened SWO serial port ==========================
    def swo_ser(self):
        """
            give a configured opened file to get SWO output. 
        """
        return serial.Serial(port    ="/dev/ttyUSB0",
                            baudrate =250000)# TODO: use environment variable or configuration value


