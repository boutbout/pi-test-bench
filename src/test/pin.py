#!/usr/bin/python3
#-*- coding: utf-8 -*-

import logging 
from time import sleep

log = logging.getLogger('gpio_class')

try:
    import RPi.GPIO as GPIO
except:
    log.warning("Not module RPi!!!! => Simulate...")
    from . import simulation_gpio as GPIO

pwm_range_max = 1024
# x2 by experience
pwm_clocks_hz = {
        1: 2342*2,
        2: 4810000*2,
        3: 3190000*2,
        4: 2398000*2,
        5: 1919000*2,
        6: 1600000*2,
        7: 1300000*2,
        8: 1200000*2,
        9: 1067000*2,
        10: 959000*2,
        11: 871000*2,
        20: 480000*2,
        200: 48000*2,
        500: 19000*2,
        1000: 9590*2,
        2000: 4802*2,
        4000: 2401*2
}

def pwm_get_clock_id(fp_hz,pwm_clk_id = None, pwm_clk_freq = None): 

    fp_range = fp_hz*pwm_range_max

    for clk_id,freq in pwm_clocks_hz.items():
        if fp_range >= freq and ( pwm_clk_freq == None or pwm_clk_freq < freq ):
            pwm_clk_freq = freq
            pwm_clk_id = clk_id
    if pwm_clk_id == None :
        pwm_clk_id = 4000
        pwm_clk_freq = pwm_clocks_hz[pwm_clk_id]

    log.debug("For %dHz we found clk %d at %dHz"\
              %(fp_hz,pwm_clk_id,pwm_clk_freq)
             )
    return (pwm_clk_id,pwm_clk_freq)


GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

def set_spi_mosi(port):
    #GPIO.setup(10,GPIO.OUT)
    pass

class pin_class(object):
    def __init__(self,pin,pol=True,delay_on=0,delay_off=0):
        self._initialised   = False
        self._pwm           = None
        self.pin            = pin # Header pin number not GPIO nbr
        self.pol            = pol
        self.delay_on       = delay_on
        self.delay_off      = delay_off

    def __str__(self):
        return "Pin %2d on=%s delay on %fs:off:%fs"%\
            (self.pin,'HIGH' if self.pol else 'LOW',self.delay_on,self.delay_off)

    def init(self):
        log.debug("Setup pin %d pol: %s delay (on: %d off:%d)"\
                  %(self.pin,self.pol,self.delay_on,self.delay_off)
                 )
        if self.pol:
            GPIO.setup(self.pin, GPIO.OUT,initial=GPIO.LOW)
        else:
            GPIO.setup(self.pin, GPIO.OUT,initial=GPIO.HIGH)
        self._initialised = True

    def deinit(self):
        log.debug("Release pin %d"%(self.pin))
        GPIO.setup(self.pin, GPIO.IN)
        self._initialised = False

    def on(self):
        if not self._initialised :
	        self.init()

        GPIO.output(self.pin, self.pol)
        sleep(self.delay_on)

    def off(self):
        if not self._initialised :
	        self.init()

        if self._pwm != None:
            self._pwm.stop()

        GPIO.output(self.pin, not self.pol)
        sleep(self.delay_off)

    def setLvl(self,on):
        if on :
            self.on()
        else:
            self.off()

    def lvl(self):
        if not self._initialised :
	        self.init()

        if self._pwm != None:
            self._pwm.stop()

        if GPIO.input(self.pin) :
            return self.pol
        return not self.pol

    def get_freq(self):
        if self._pwm != None :
            return self._pwm["freq"]
        return -1

    def pwm(self,freq,duty=50):
        if self.pin == 12:
            import os
            log.debug("Use hardware pwm wiring pi pin 1")
            if self._pwm == None or self._pwm["freq"] != freq:
                clk_id,clk_freq=pwm_get_clock_id(freq)

                if freq != 0 :
                    s = int(clk_freq/freq)
                else:
                    s = 0
                self._pwm = {"freq":freq,"s":s}

                os.system("gpio mode 1 pwm") #don't forget to set it in PWM mode 
                os.system("gpio pwm-ms") # m => high level time, s preriod size
                os.system("gpio pwmc %d"%(clk_id))
                os.system("gpio pwmr %d"%(s))


            m = int(self._pwm["s"]*duty/100)
            log.debug("M = %d , S = %d (freq %dHz)"\
                      %(m,self._pwm["s"],self._pwm["freq"]))
            os.system("gpio pwm 1 %d"%(m))

        else:
            log.debug("Use software pwm")
            if not self._initialised : 
                self.init()
            if self._pwm == None:
                if freq:
                    self._pwm = GPIO.PWM(self.pin, freq)
                    self._pwm.start(duty) 
            else:
                if freq:
                    self._pwm.ChangeFrequency(freq)
                else:
                    self._pwm.Stop()
            



