#!/usr/bin/python3
#-*- coding: utf-8 -*-

import os
import sys
import logging 
import tempfile
 
import errno
import serial
import subprocess

log = logging.getLogger('test.base')

class files(object):
    def __init__(self, cfgname_):
        self.cfgname = cfgname_

    def filepath(self,fname):
        """ give absolute path to store filename 'fname' """
        return os.path.join(self.dirpath(), fname)

    def dirpath(self):
        """ give absolute path to store file for this configuration """
        path = os.path.join(tempfile.gettempdir(), "ptb-%s"%(self.cfgname))
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
        return path

    def add(self,fname,data):
        """ Add new file name with data content """
        fh = open(self.filepath(fname), 'wb')
        fh.write(data)
        fh.close()

    def remove(self,fname):
        """ Remove file name file """
        try:
            os.remove(self.filepath(fname))
        except OSError:
            pass

    def list(self):
        """
            give list of file can be used for this configuration
            return a list with:
                * name : file name without path.
                * size : file size in bytes.
                * date : file modification date in second since epoch.
        """
        l = []
        for root,folder,files in os.walk(self.dirpath()):
            for name in files:
                path = os.path.join(root,name)
                e = os.stat(path)
                info = {"name":name,"size":e.st_size,"date":e.st_mtime}
                log.debug("file : %s"%(str(info)))
                l.append(info)
            break;
        return l

class baseTestClass(object):

    def __init__(self,cfgname):
        self.ON_POSIX = 'posix' in sys.builtin_module_names
        self.cfgname = cfgname
        self._elements = {}

        self.files = files(cfgname)

        """ 
            ElementGroup should be change in a baseElement class by inherited
            class
        """
        self.elementGroup = None

        
        pass

    def start(self):
        """
            Test class is ready
        """
        for key,e in self._elements.items():
            log.debug('Starting %s'%(self.cfgname))
            e.start()


    def shells_list(self):
        return [
            ("swo","fd",self.swo_ser),
            ("gdb","cmd",self.gdbserver_process,5)
        ]

    def dashboard(self):
        return self.elementGroup.dashboard()

    def get_element(self,name):
        return self._elements.get(name,None)

    def kill(self):
        """
            test class will destroyed
        """
        for key,e in self._elements.items():
            log.debug('Killing %s'%(self.cfgname))
            e.kill()

    def addElement(self,e,direct=True):
        self._elements[e.pathName] = e
        e.testClass = self


    def gdbserver_process(self):
        """
            give a configured gdb server subprocess 
        """
        return subprocess.Popen(["gdbserver"],bufsize=1,
                                stdin =subprocess.PIPE, 
                                stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, 
                                close_fds=self.ON_POSIX
                               )

    def swo_ser(self):
        """
            give a configured opened file to get SWO output. 
        """
        return serial.Serial(port="/dev/ttyAMA0",baudrate=115200)



