
import os
import threading
import logging 

from time import sleep, time, gmtime, strftime

from . import ad9850
from .pin import pin_class

log = logging.getLogger('test.elements')

"""
dashboard description:
    list of graphic element with :
        * name      : attribute name of test class.
        * label     : Label showed on web interface.
        * readonly  : if true, this attribute cannot be only read.
        * type      : one of this string value
            - bool      => true/false attribute
            - int       => integer value attribute
            - enum      => enum attribute. List of value is in 'enumlist'
            - action    => action button, no value
            - screen    => graphic zone
            - group     => group together list of graphic element listed in
                           'content'
            - shell     => console view
            - file      => a file
            - string    => a string
        * min       : minimum integer value or string length.
        * max       : maximum integer value or string length.
        * enumlist  : dict that associate attribute value and showed string.
        * content   : list of element for group type
"""

class baseElement(object):
    """ element type use in reactJs """
    typeName    = None

    def __init__(self, parent, name_, label_, *args, **kwargs):

        try:
            self.pathName   = parent.pathName+'.'+name_
        except AttributeError:
            self.pathName   = 'root.'+name_

        self.name   = name_ 
        self.parent = parent
        self.label  = label_
        self.defVal = kwargs.pop('defVal', None)
        self.val    = None
        self._observers = []
        self.testClass  = None
        self.lock       = threading.RLock()

        self.parent.addElement(self)

    def addObserver(self,observer):
        self._observers.append(observer)

    def addElement(self,element,direct=True):
        self.parent.addElement(element,False)

    def start(self):
        """
            Test class is ready this element start on test bench
        """
        log.debug('Starting %s'%(self.name))
        self.set(self.defVal)
        pass

    def kill(self):
        """
            Set this element will destroyed
        """
        log.debug('Killing %s'%(self.name))
        pass

    def dashboard(self):
        """
            return dashboard description for this element
        """

        return {
            'name' : self.pathName,
            'label': self.label if self.label != None else self.name,
            'type' : self.typeName
        }

    def set(self, val):
        """
            set value of this element
        """
        val = self.checkVal(val)
        if val != self.val:
            self.applyVal(val,)
        pass

    def get(self):
        """
            get value of this element
        """
        return self.val

    def checkVal(self, val):
        """
            check new value and return saturated value
        """
        return val

    def applyVal(self, val):
        """
            Apply val to test bench
        """
        old = self.val
        self.val = val
        for observer in self._observers:
            observer(self,old,val)


class baseFile(baseElement):
    typeName = 'file'

    def checkVal(self, val):
        val = super().checkVal(val)

        if val and not os.path.isfile(self.testClass.files.filepath(val)):
            log.error("File not exist %s"%(val))
            val = self.val

        return val


class baseBool(baseElement):
    typeName = 'bool'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.defVal == None:
            self.defVal = False

    def checkVal(self, val):
        val = super().checkVal(val)
        return bool(val)

class baseGroup(baseElement):
    typeName = 'group'
    def __init__(self, *args, **kwargs):
        self._content = []
        super().__init__(*args, **kwargs)

    def addElement(self,element,direct=True):
        super().addElement(element,direct)
        if direct :
            self._content.append(element)

    def dashboard(self):
        d = super().dashboard()
        l = []
        for e in self._content:
            l.append(e.dashboard())
        d['content'] = l
        return d

class pinOutputTrmin(baseGroup):
    def __init__(self, *args, **kwargs):
        pinCfg = kwargs.pop('pinCfg')
        super().__init__(*args, **kwargs)
        self.pin = pin_class(**pinCfg)
        self.rpm_pulse_per_tr = baseNumber(self,'rpm_pulse_per_tr', 'Pulse/Tr for RPM', 
                                        valMin=1, valMax=4, step=0.5)
        self.trmin = baseNumber(self,'trmin', 'Tr/Min', 
                                valMin=0, valMax=40000, step=100)

        self.rpm_pulse_per_tr.addObserver(self.onChange)
        self.trmin.addObserver(self.onChange)

    def start(self):
        self.pin.init();
        super().start()

    def kill(self):
        super().kill()
        self.pin.deinit();

    def onChange(self,elem,oldVal,val):
        freq = 0
        if ( self.trmin.val != None and 
             self.rpm_pulse_per_tr.val != None and
             self.rpm_pulse_per_tr.val > 0 
           ):

            freq=float(self.trmin.val)/60.*float(self.rpm_pulse_per_tr.val)

        self.pin.pwm(freq)

class baseKeyboard(baseGroup):

    def __init__(self, *args, **kwargs):
        self.keys        = kwargs.pop('keys')
        super().__init__(*args, **kwargs)

        for name in self.keys:
            key = baseBool(self,name,name)
            key.addObserver(self.keychanged)

    def keychanged(self,elem,old,val):
        """ to be overrode """
        print(elem.name,val)
        pass

class baseNumber(baseElement):
    typeName    = 'number'

    def __init__(self, *args, **kwargs):
        self.valMax = kwargs.pop('valMax', None)
        self.valMin = kwargs.pop('valMin', None)
        self.step   = kwargs.pop('step', 1)
        super().__init__(*args, **kwargs)
        if self.defVal == None:
            self.defVal     = 0

    def dashboard(self):
        d = super().dashboard()
        d['step'] = self.step
        d['max'] = self.valMax
        d['min'] = self.valMin
        return d

    def checkVal(self, val):
        val = super().checkVal(val)
        if self.valMax != None and self.valMax < val:
            val = self.valMax
        if self.valMin != None and self.valMin > val:
            val = self.valMin
        val = int(val/self.step)*self.step
        return val

class pinOutput(baseBool):
    def __init__(self, *args, **kwargs):
        pinCfg = kwargs.pop('pinCfg')
        super().__init__(*args, **kwargs)
        self.pin = pin_class(**pinCfg)

    def start(self):
        self.pin.init();
        super().start()

    def kill(self):
        super().kill()
        self.pin.deinit();

class pinOutputPP(pinOutput):
    def applyVal(self, val):
        self.pin.setLvl(val)
        super().applyVal(val)

class pinOutputPulse(pinOutput):
    def applyVal(self, val):
        self.pin.setLvl(True)
        self.pin.setLvl(False)
        super().applyVal(False)

class pinOutputPwm(baseNumber):
    def __init__(self, *args, **kwargs):
        self.rpm_pulse_per_tr = kwargs.pop('rpm_pulse_per_tr', 1)
        pinCfg = kwargs.pop('pinCfg')
        super().__init__(*args, **kwargs)
        self.pin = pin_class(**pinCfg)

    def start(self):
        self.pin.init();
        super().start()

    def kill(self):
        super().kill()
        self.pin.deinit();

    def applyVal(self, val):
        freq=float(val)/60.*float(self.rpm_pulse_per_tr)
        self.pin.pwm(freq)
        super().applyVal(val)

class basePeriodicalThread(baseGroup):
    def __init__(self, *args, **kwargs):
        self.sync       = kwargs.get('sync',True)
        super().__init__(*args, **kwargs)
        self.thread     = threading.Thread()

        self.period_ms  = baseNumber (self,'period_ms','Period (mS)',
                                      valMin=1, defVal=1000)
        self.running    = baseBool(self,'play','Play')

        self.fd_out     = None
        self.quit       = False
        self.thread.run = self.run

    def start(self):
        self.thread.start()

    def kill(self):
        if self.lock.acquire(timeout=10):
            self.quit = True
            self.lock.release()
        if self.thread.isAlive():
            self.thread.join()

    def run(self):
        t = time()
        while not self.quit:

            if not self.running.val :
                sleep(1)
                continue;

            if self.sync:
                t = int((t*1000/self.period_ms.val)+1)/1000*self.period_ms.val
            else:
                t += float(self.period_ms.val)/1000

            wait = t-time()
            if wait > 1 :
                sleep(1)
                continue
            elif wait > 0:
                sleep(wait)

            if not self.running.val :
                continue;

            if self.lock.acquire(timeout=10):
                try:
                    t = time()
                    self.update()
                except:
                    log.exception('file_player failed')
                finally:
                    self.lock.release()

    def update(self):
        """
            overrode function called at each period
        """
        raise Exception("Must be overrode")


class player(basePeriodicalThread):
    def __init__(self, *args, **kwargs):
        self.openOut    = kwargs.get('openOut')
        super().__init__(*args, **kwargs)

        self.pos        = baseNumber (self,'pos','Pos')
        self.fname      = baseFile(self,'file','File')

        self.fd_out     = None
        self.fd_in      = None
        self.openFname  = None

    def kill(self):
        super().kill()
        if self.fd_out:
            self.fd_out.close()
        if self.fd_in:
            self.fd_in.close()

    def update(self):
        if self.pos.val == 0:
            if self.fd_in : 
                self.fd_in.seek(0)

        if self.fd_in and self.openFname != self.fname.val:
            self.fd_in.close()
            self.fd_in = None

        if not self.fd_in :
            self.openFname = self.fname.val
            if self.openFname:
                path = self.testClass.files.filepath(self.openFname)
                self.fd_in = open(path)
        if not self.fd_out :
            self.fd_out = self.openOut()
        if self.fd_in and self.fd_out:

            l = self.fd_in.readline()
            self.fd_out.write(l.encode('utf-8'))

            pos = self.pos.val

            if not pos:
                pos = 1
            else:
                pos += 1

            self.pos.set(pos)

            log.debug('file_player pos %d out:%s'\
                      %(pos,str(l).rstrip("\n\r"))
                     )

class fakeGPS(basePeriodicalThread):
    def __init__(self, *args, **kwargs):
        self.openOut    = kwargs.get('openOut')
        super().__init__(*args, **kwargs)

        self.latitude   = baseNumber(self,'latitude','Lat',
                                     valMin=-90, valMax=+90, step=0.000001)
        self.longitude  = baseNumber(self,'longitude','Lon',
                                     valMin=-180,valMax=+180, step=0.000001)
        self.Speed      = baseNumber(self,'Speed','Speed',
                                     valMin=0, valMax=400, step=0.1)
        self.Heading    = baseNumber(self,'Heading','Heading',
                                     valMin=0, valMax=+360, step=0.1)

        self.fd_out     = None
        self.openFname  = None

    def kill(self):
        super().kill()
        if self.fd_out:
            self.fd_out.close()

    def nmea_crc(self,frame):
        crc = 0
        for s in frame:
            crc ^= ord(s)
        return crc



    def build_frame(self,t):
        tm = gmtime(t)
        l = [
            "GPRMC",
            strftime("%H%M%S",tm)+".%03d"%(int(t*1000)%1000),
            "A"
            ]

        f = self.latitude.val
        d = 'N'
        if ( f < 0 ):
            d = 'S'
            f = -f
        i = int(f)
        f -= i;
        f *= 60
        l.append("%02d%06.3f"%(i,f))
        l.append(d)

        f = self.longitude.val
        d = 'W'
        if ( f < 0 ):
            d = 'E'
            f = -f
        i = int(f)
        f -= i;
        f *= 60
        l.append("%03d%06.3f"%(i,f))
        l.append(d)
        
        f = self.Speed.val
        f /= 1.852
        l.append("%05.1f"%(f))

        f = self.Heading.val
        l.append("%05.1f"%(f))

        l.append(strftime("%d%m%y",tm))

        l.append("%05.1f"%(0))  # TODO Variation value
        l.append("E")           # TODO Variation direction

        s = ",".join(l)

        # test frame
        #s = "GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W" # crc = 6A
        crc = self.nmea_crc(s)

        return "$"+s+"*%02X"%crc


    def update(self):
        if not self.fd_out :
            self.fd_out = self.openOut()
        if self.fd_out:

            l = self.build_frame(time())
            self.fd_out.write((l+"\n").encode('utf-8'))

            log.debug('file_player out:%s'%(l))

        

