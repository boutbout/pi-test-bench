#!/usr/bin/python3
#-*- coding: utf-8 -*-

import logging 
import struct
from time import sleep

log = logging.getLogger('ad9850_class')

from . import pin

simulation = False
try:
    import spidev
except:
    log.warning("Not module spidev!!!! => Simulate...")
    simulation = True

ad9850_clock_freq_hz=125000000
ad9850_reset_pin= pin.pin_class(22,
                                delay_on =(5/ad9850_clock_freq_hz), # 5clk
                                delay_off=(2/ad9850_clock_freq_hz)  # 2clk
                               )
ad9850_spi_port=0
ad9850_spi_cs=0


#I cannot set to alternate function with python ...
import os
# TODO set alternate pin without raspi-gpio 
os.system('raspi-gpio set 10 a0')

def config(phase,control,freq):
    if simulation :
        log.info("phase = %d control = %d freq = %d"%(phase,control,freq))
        return
    cfg  = 0 # lease 0 for ad9850
    cfg  = 0x0 # lease 0 for ad9850
    freq = freq * 0xFFFFFFFF/ad9850_clock_freq_hz
    data = struct.pack(">BL",cfg,int(freq))
    data = struct.unpack("BBBBB",data)
    data = list(data)
    for j,val in enumerate(data):
        ret=0
        for i in range(8):
            if val & (1<<i):
                ret |= 1<<(7-i)
        data[j] = ret

    log.debug("data: %02X %02X %02X %02X %02X"\
              %(data[4],data[3],data[2],data[1],data[0]))
    pin.set_spi_mosi(ad9850_spi_port)
    spi = spidev.SpiDev()
    spi.open(ad9850_spi_port,ad9850_spi_cs)
    try:
        spi.xfer2([data[4],data[3],data[2],data[1],data[0]])
    finally:
        spi.close()
    

ad9850_reset_pin.init()
ad9850_reset_pin.on()
ad9850_reset_pin.off()
config(0,0,0)

def sine(freq):
    config(0,0,freq);
    

