#!/usr/bin/python3
#-*- coding: utf-8 -*-

import os
import errno
import tempfile
import tornado.web as web
import tornado.escape
import tornado.websocket
import logging 

log = logging.getLogger('statesocket')

class handler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    @classmethod
    def stop(cls):
        pass

    def open(self):
        handler.waiters.add(self)
        if 'config' in handler.cache:
            for key in handler.cache['config']:
                self.send(self,'config',key)

    def on_close(self):
        handler.waiters.remove(self)

    @classmethod
    def set(cls, sect, key, value):

        if not sect in cls.cache:
            cls.cache[sect] = {}

        cls.cache[sect][key] = value

        for waiter in cls.waiters:
            cls.send(waiter,sect,key)

    @classmethod
    def cfg_file_path(cls,fname):
        path = os.path.join(os.path.expanduser("~"), "ptb", "cfg")
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
        return os.path.join(path,fname)

    @classmethod
    def set_save(cls,fname):
        if fname == "":
            fname = "default"

        try:
            path = cls.cfg_file_path(fname)
            json = tornado.escape.json_encode(cls.cache)
            f = open(path,"w")
            f.write(json)
            f.close()
        except:
            log.exception('Save configuration failed')

    @classmethod
    def get_save(cls,fname):
        cache = {}
        if not fname or fname == "" :
            fname = "default"

        try:
            path = cls.cfg_file_path(fname)
            f = open(path,"r")
            json = f.read()
            f.close()
            cache = tornado.escape.json_decode(json)
            cls.cache = {}
        except:
            log.exception('Load configuration failed')

        return cache
            

    @classmethod
    def send(cls, waiter, sect ,key):
        try:
            value = cls.cache[sect][key]
            log.info("send %s : %s = %s"%(sect,key,value))
            msg = {'sect':sect,'key':key,'value':value}
            json = tornado.escape.json_encode(msg)
            waiter.write_message(json)
        except:
            logging.error("Error sending json", exc_info=True)

    def on_message(self, msg):
        error = None
        changed = None

        logging.info("got message %r", msg)
        try:
            req     = tornado.escape.json_decode(msg)
            sect    = req.pop('sect','test')
            key     = req.pop('key')
            value   = req.get('value',None)
            self.application.cmd(sect,key,value)
        except Exception as e:
            log.exception("command failed %s"%(msg))
            error = str(e)

        if error:
            json = tornado.escape.json_encode({'error':error})
            self.write_message(json)

