#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys
import logging 

import tornado.ioloop
import tornado.web as web
import tornado.escape
import tornado.websocket
from threading import Thread,RLock
import importlib
import select
import time
import subprocess

from queue import Queue, Empty  # python 3.x

ON_POSIX = 'posix' in sys.builtin_module_names
log = logging.getLogger('shellsocket')

class _stream(object):
    def __init__(self,fd,name=""):
        self.name   = name
        self.lock   = RLock()
        self.fd     = fd
        self.q      = Queue()

        self.t      = Thread(target=self._enqueue_output)
        self.t.daemon = True # thread dies with the program
        self.t.start()

    def _enqueue_output(self):
        """
            TODO: allow to filter data with SWO prefix.
        """
        for line in iter(self.fd.readline, b''):
            line = bytes(filter(lambda x: ( x >= ord(' ') or x == 27 ),line))
            line = line.decode("ascii",'ignore') # TODO: use environment variable or configuration value
            self.q.put(line)
        with self.lock:
            self.kill()

    def kill(self):
        with self.lock:
            self.fd = None
            self.t = None
            self.q.put("CLOSED".center(40,"-"))

    def isAlive(self):
        return (self.fd != None)

    def read(self):
        line = None
        with self.lock:
            if self.q:
                try:  # read line without blocking
                    line = self.q.get_nowait() # or q.get(timeout=.1)
                    log.debug("Read(%s):%s"%(self.name,line))
                except Empty:
                    pass
        return line
            


class shell_class(object):
    def __init__(self,name,mode,func,restartDelay = 0):
        self.name       = name
        self.mode       = mode
        self.func       = func
        self.size       = 200
        self.lines      = []
        self.cmd        = None
        self.streams    = []
        self.restartTime = None
        self.restartDelay = restartDelay

    def __str__(self):
        return "Name:%s mode:%s func:%s"\
            %( self.name, self.mode, str(self.func))

    def add_line(self,line):
        self.lines.append(line)
        if len(self.lines) > self.size:
            self.lines = self.lines[-self.size:]
        handler.add_line(self.name,line)

    def clear(self):
        self.lines=[]

    def restart(self):
        self.stop()
        self.add_line("Restarted".center(40,"-"))
        self.start()

    def start(self):
        log.info("Starting shell %s"%(self.name))
        fd_out = None
        fd_err = None
        try:
            if self.mode == "cmdline":
                self.cmd = subprocess.Popen(self.func(),bufsize=1, 
                                            stdin =subprocess.PIPE, 
                                            stdout=subprocess.PIPE, 
                                            stderr=subprocess.PIPE, 
                                            close_fds=ON_POSIX
                                           )
                fd_out  = self.cmd.stdout
                fd_err  = self.cmd.stderr

            elif self.mode == "cmd":
                self.cmd = self.func()
                fd_out  = self.cmd.stdout
                fd_err  = self.cmd.stderr

            elif self.mode == "fd":
                fd_out  = self.func()

            if fd_out:
                self.streams.append(_stream(fd_out,"stdout"))
            if fd_err:
                self.streams.append(_stream(fd_err,"stderr"))
            self.add_line("Started".center(40,"-"))

        except:
            log.exception("shel class run failed")
            self.stop()

        if fd_out == None and fd_err == None:
            self.add_line("Failed".center(40,"-"))

    def stop(self):
        log.info("Stopping shell %s"%(self.name))
        if self.cmd:
            self.cmd.kill()
        for stream in self.streams:
            stream.kill()
        self.streams = []

    kill = stop

    def loop(self):
        try:
            if self.restartTime:
                if self.restartTime < time.time():
                    self.restart()
                    self.restartTime = None
                else:
                    return

            alive = False
            for stream in self.streams:
                if stream.isAlive() :
                    alive = True
                while True:
                    line = stream.read()
                    if not line:
                        break
                    self.add_line(line)
            if not alive and self.restartDelay:
                m = "restart in %s second"%(self.restartDelay)
                self.add_line(m.center(40,"-"))
                self.restartTime = time.time() + self.restartDelay
        except:
            log.exception("shel class run failed")
            self.stop()


class handler(tornado.websocket.WebSocketHandler):
    waiters     = set()
    shell       = {}
    periodic    = None

    @classmethod
    def _loop(cls):
        for name in cls.shell:
            cls.shell[name].loop()
            
    @classmethod
    def add_shell(cls,*args,**kwargs):
        name = kwargs.pop('name',args[0])
        log.info("Adding shell %s"%(name))
        if name in cls.shell:
            log.exception("Thread exist %s"%(name))
            return
        cls.shell[name]=shell_class(*args,**kwargs)

    @classmethod
    def remove_all(cls):
        log.info("Stopping all shell")
        try:
            for name,item in cls.shell.items():
                log.info("remove shell %s"%(name))
                item.kill()
                log.info("Done!")
        finally:
            cls.shell = {}

        if cls.periodic != None and cls.periodic.is_running():
            cls.periodic.stop()

    @classmethod
    def start_all(cls):
        log.info("stating all shell")
        for name in cls.shell.keys():
            cls.shell[name].start()

        if cls.periodic == None:
            cls.periodic = tornado.ioloop.PeriodicCallback(cls._loop, 100)

        cls.periodic.start()

    @classmethod
    def stop_all(cls):
        log.info("Stopping all shell")
        for name in cls.shell.keys():
            cls.shell[name].stop()

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self):
        self.waiters.add(self)
        for name,shell in self.shell.items():
            for line in shell.lines:
                self.send(self,name,line)

    def on_close(self):
        self.waiters.remove(self)

    @classmethod
    def clear_shell(cls,name):
        cls.shell[name].clear()
        for waiter in cls.waiters:
            cls.send(waiter,name,cmd='clear')

    @classmethod
    def restart_shell(cls,name):
        cls.shell[name].restart()
        for waiter in cls.waiters:
            cls.send(waiter,name,cmd='restart')

    @classmethod
    def add_line(cls,name, line):
        for waiter in cls.waiters:
            cls.send(waiter,name,line)

    @classmethod
    def send(cls, waiter, name, line=None, cmd='addLine'):
        try:
            msg = {'name':name, 'line':line, 'cmd':cmd}
            json = tornado.escape.json_encode(msg)
            logging.debug("sending json "+json)
            waiter.write_message(json)
        except:
            logging.error("Error sending json", exc_info=True)

    def on_message(self, msg):
        logging.info("got message %r", msg)
        try:
            parsed = tornado.escape.json_decode(msg)
            self.cmd(**parsed)
        except Exception as e:
            log.exception("command failed %s"%(msg))

    def cmd(self, *args, **kwargs):
        cmd = kwargs.pop('cmd')
        name = kwargs['name']
        if cmd == "clear":
            self.clear_shell(name)
        elif cmd == "restart":
            self.restart_shell(name)
        else:
            logging.info("Command ignored '%s' : %s"%(str(cmd),kwargs))





