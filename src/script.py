#!/usr/bin/python3
#-*- coding: utf-8 -*-

import argparse
import os
import sys
import importlib
import traceback
import logging 

log = logging.getLogger()

if __name__ == "__main__":
    try:
        import colorlog

        formater = colorlog.ColoredFormatter(
            "%(log_color)s%(levelname)-8s%(asctime)s%(reset)s %(message)s",
            datefmt=None,
            reset=True,
            log_colors={
                'DEBUG':    'cyan',
                'INFO':     'green',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            },
            style='%'
        )

        handler = logging.StreamHandler()
        handler.setFormatter(formater)

        logger = colorlog.getLogger()
        logger.addHandler(handler)
    except:
        FORMAT = '[%(asctime)s] %(message)s'
        logging.basicConfig(format=FORMAT)
        log.exception('log color failed')


parser = argparse.ArgumentParser(description='Pi-test bench.')

parser.add_argument('--project', help='project directory',
                    default=os.environ.get('PROJECT_DIR','..')
                   )
parser.add_argument('--script',  help='test script name : file in ressources folder without ".txt"'
                   )
parser.add_argument('-v', '--verbose', help="verbose level... repeat up to three times.", 
                    action="count"
                   )
parser.add_argument('testList',     help='lists of test to do', 
                    metavar='N', type=str, nargs='*'
                   )

parser.add_argument('--host',     help='webservice host',
                    default=os.environ.get('PTB_HOST',"localhost")
                   )
parser.add_argument('--port',     help='webservice port',
                    default=os.environ.get('PTB_PORT',"8888")
                   )

parser.add_argument('--gdbchost',  help='webservice host',
                    default=os.environ.get('PTB_GDBC_HOST',"localhost")
                   )
parser.add_argument('--gdbcport',  help='webservice port',
                    default=os.environ.get('PTB_GDBC_PORT',"8889")
                   )

parser.add_argument('--no_ptb',     help='no pi test bench service',
                    default=False,  action="store_true"
                   )

args = parser.parse_args()

def _websock_ask(url,*args,**kwargs):
    ret = None
    try:
        import tornado.escape
        from websocket import create_connection
        ws = create_connection(url)
        json = tornado.escape.json_encode(kwargs)
        log.debug("Sending :%s..."%(json))
        ws.send(json)
        json =  ws.recv()
        log.debug("Received :%s..."%(json))
        ret = tornado.escape.json_decode(json)
        ws.close()
    except:
        log.exception("_websock_ask failed url:%s args:%s"%(url,str(kwargs)))
    return ret

if not args.verbose:
    log.setLevel('ERROR')
elif args.verbose == 1:
    log.setLevel('WARNING')
elif args.verbose == 2:
    log.setLevel('INFO')
elif args.verbose >= 3:
    log.setLevel('DEBUG')
else:
    log.critical("UNEXPLAINED NEGATIVE COUNT!")

topDir = os.path.dirname(os.path.split(os.path.realpath(__file__))[0])
log.debug("topDir:"+topDir)

test_list = []

log.debug(str(args.script))
if args.script:
    script_path = os.path.join(topDir,
                               'ressources/test-script',
                               args.script
                              )
    log.debug("script_path:"+script_path)
    test_list = open(script_path).readlines()
    test_sep = ' '

elif len(args.testList) != 0:
    test_list = args.testList
    test_sep = '|'

ws_url          = "ws://%s:%d/gdbc"%(str(args.host    ),int(args.port    ))
http_url        = "http://%s:%d/"  %(str(args.host    ),int(args.port    ))
ws_gdbc_url     = "ws://%s:%d/gdbc"%(str(args.gdbchost),int(args.gdbcport))
http_gdbc_url   = "http://%s:%d/"  %(str(args.gdbchost),int(args.gdbcport))

log.info("web service url pi test bench : '%s'"%(ws_url))
log.info("web service url GDBClient     : '%s'"%(ws_gdbc_url))

if not args.no_ptb :
    _websock_ask(ws_url,cmd="gdbcwebsocket-url",value=ws_gdbc_url)

for test_line in test_list:
    test_line = test_line.translate(None,'\n\r').strip()
    if test_line == '' or test_line[0] == '#':
        continue
    s = "================= Test %s ==================" % test_line
    log.info(s.ljust(80,'='))
    try:
        # with filter remove empty argument
        func_args = filter(None,test_line.split(test_sep)) 
        log.debug(str(func_args))
        if func_args[0] == 'gdbc' :
            args = {
                'sect':func_args[1],
                'func':func_args[2],
                'args':func_args[3:]
            }
            ret = _websock_ask(ws_gdbc_url,**args)

            log.info("Request : %s"%(str(ret['req'])))
            log.info("Answer  : %s"%(str(ret['ret'])))

        elif func_args[0] == 'post' :
            import requests
            dest = func_args[1]
            fname = os.path.expandvars(os.path.expanduser(func_args[2]))
            files = {'filearg': open(fname, 'rb')}

            if dest == 'gdbc':
                r = requests.post(http_gdbc_url, files=files)
            else:
                r = requests.post(http_gdbc_url, files=files)

            log.info("Send    : %s"%(fname))
            log.info("Answer  : %s"%(str(r)))

        else:
            ret = _websock_ask(ws_url,func_args)
            log.info("Answer  : %s"%(str(ret)))

    except Exception as e :
        log.exception("Test '%s' Failed Reason %s"%(test_line,str(e)))
        exit(1)



