#!/usr/bin/python3
#-*- coding: utf-8 -*-

import os
import sys
import time
import logging
import subprocess

from threading import Thread,RLock
from fcntl import fcntl, F_GETFL, F_SETFL

import tornado.ioloop


try:
    from Queue import Queue, Empty
except ImportError:
    from queue import Queue, Empty  # python 3.x

ON_POSIX = 'posix' in sys.builtin_module_names
log = logging.getLogger()

try:
    import TimeoutError
except ImportError:
    class TimeoutError(OSError):
        pass

class gdbc(object):
    def __init__(self,app,path):
        self.cmd    = None
        self.thread = None
        self.app    = app
        self.path   = None
        self._read_buf    = ''
        self.readlines = []
        self.prompt_str = '(gdb) '
        self.lock   = RLock()
        self.elf    = {
            'flash':None,
            'symbol':None
        }
            
        if path:
            self.start_gdb(path)

    def start_gdb(self,path):
        cmd = None
        try:
            cmd = subprocess.Popen(path.split(' '),
                                   stdin =subprocess.PIPE, 
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.STDOUT, 
                                   close_fds=ON_POSIX
                                      )
            flags = fcntl(cmd.stdout, F_GETFL) # get current p.stdout flags
            fcntl(cmd.stdout, F_SETFL, flags | os.O_NONBLOCK)
        except:
            log.exception("gdbc Exception:")
            if cmd:
                cmd.kill()
            raise

        log.info('Gdb client started')
        self.cmd    = cmd
        self.confirm(False)

    def kill(self):
        print"===================="
        log.info("Killing gdb client")
        if self.cmd:
            self.cmd.kill()

    def loop(self):
        with self.lock:
            self._flush()

    def _read(self,timeout=0.1):
        t = time.time() + timeout
        while True:
            try:
                c = self.cmd.stdout.read(1)
            except IOError:
                if t < time.time():
                    raise TimeoutError
                time.sleep(0.1)
                continue

            if c == '\n':
                s = self._read_buf 
                if s != '':
                    self.readlines.append(s)
                self._read_buf = ''
                log.debug("Read(gdb) '%s'"%(s))
                return False

            if self._read_buf == True:
                self._read_buf = ''

            self._read_buf += c
            if self._read_buf == self.prompt_str:
                self._read_buf = ''
                log.debug("Read(gdb) Prompt")
                return True

    def _write(self,cmd):
        self._read_buf = ''
        log.debug("write:'%s'"%(cmd))
        line = cmd+'\n'
        self.cmd.stdin.write(line)

    def _flush(self):
        while True:
            try: 
                self._read(0)
                continue
            except TimeoutError:
                pass

            if False:
                self._write('')
                self._wait_prompt()
                n = len(self.readlines)
                if n == 0:
                    break;

                for l in self.readlines:
                    log.debug("flushed:'%s'"%(l))
            self.readlines = []
            break

    def _wait_prompt(self,timeout=60):
        log.debug("Waiting Prompt")
        if self._read_buf == True:
            return
        while True:
            line = self._read(timeout)
            if line == True:
                return


    def execute(self,cmd):
        ret = []
        line = cmd.split('\n')[0]

        with self.lock:
            time.sleep(0.1)
            self._flush()

            self._write(cmd)
            self._wait_prompt()

            log.debug("execute: %s"%(cmd))
            for l in self.readlines:
                log.debug("> %s"%(l))

            ret = self.readlines
            self.readlines = []

        return ret
        
    def run(self):
        ret = self.execute("continue&")
        return ret

    def stop(self):
        ret = self.execute("interrupt")
        time.sleep(1)
        self._flush()
        return ret

    def reset(self):
        ret = self.execute("monitor reset init")
        return ret

    def flash(self):
        ret = self.execute("load %s"%(self.elf['flash']))
        return ret

    def symbol(self):
        ret = self.execute("file %s"%(self.elf['symbol']))
        return ret

    def connection(self,ip,port):
        ret = self.execute("target remote %s:%s"%(ip,port))
        return ret

    def confirm(self,on):
        if on:
            cmd = "set confirm on"
        else:
            cmd = "set confirm off"
        ret = self.execute(cmd)
        return ret

    def getvar(self,name):
        ret = self.execute("print %s"%(name))
        return ret


        
