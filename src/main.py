#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""
    Main web pi test bench server
"""

import logging

LOG = logging.getLogger()

if __name__ == "__main__":
    try:
        import colorlog

        FORMATER = colorlog.ColoredFormatter(
            "%(log_color)s%(levelname)-8s%(asctime)s%(reset)s %(message)s",
            datefmt=None,
            reset=True,
            log_colors={
                'DEBUG':    'cyan',
                'INFO':     'green',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            },
            style='%'
        )

        HANDLER = logging.StreamHandler()
        HANDLER.setFormatter(FORMATER)
        colorlog.getLogger().addHandler(HANDLER)

    except ImportError:
        FORMAT = '[%(asctime)s] %(message)s'
        logging.basicConfig(format=FORMAT)
        LOG.exception('log color failed')

import argparse
import os
import sys
from importlib import import_module

import shellsocket
import statesocket

import tornado.ioloop
import tornado.web as web

class MainHandler(web.RequestHandler):
    """ main page handler """
    def get(self):
        self.render("index.html",
                    cfg_name=self.application.cfg_name,
                    cfg_list=self.application.cfg_list)

    def post(self):
        try:
            changed = None

            fileinfo = self.request.files['filearg'][0]

            if self.application.test:
                name = fileinfo['filename']
                body = fileinfo['body']
                self.application.test.files.add(name, body)
            else:
                LOG.error("no test selected")

            self.application.cmd('files', 'list', None)
        except:
            LOG.exception('Post error')

        self.get()
        #self.finish("Uploaded %s" %path)

class loglevel(object):
    """ Manage log level configuration """
    def __init__(self, app):
        self.app = app
        self.levelName = None

    def get(self):
        return self.levelName

    def set(self, value):
        """
            set Application log level
            value: wanted log level
                0 or CRITICAL
                1 or ERROR
                2 or WARNING
                4 or INFO
                5 or DEBUG
                6 or NOTSET
        """
        l = [
            'CRITICAL',
            'ERROR',
            'WARNING',
            'INFO',
            'DEBUG',
            'NOTSET'
        ]

        if value.isdigit():
            value = l[int(value)]

        LOG.setLevel(value)
        self.levelName

class cfgname(object):

    def __init__(self, app_):
        self.app = app_

    def set(self, cfg_name):
        """
        set Application test configuration name
            cfg_name: configuration wanted
        """
        test = self.app.test

        self.app.test = None
        self.app.cfg_name = None

        try:
            if test != None:
                test.kill()
        except Exception:
            LOG.exception("failed to deselect")

        shellsocket.handler.remove_all()

        test = None

        try:
            if cfg_name != '---':

                testClass = import_module('test.cfg.'+cfg_name)

                test = testClass.obj(cfg_name)

                shells_list = test.shells_list()
                for shell in shells_list:
                    shellsocket.handler.add_shell(*shell)
                shellsocket.handler.start_all()

        except:
            if test != None:
                test.kill()
            test = None
            LOG.error("Cannot load %s" %(cfg_name))
            raise

        if test != None:
            test.start()
        else:
            cfg_name = None

        self.app.test = test
        self.app.cfg_name = cfg_name

    def get(self):
        """ Give current selected configuration name. """
        return self.app.cfg_name

class dashboard(object):
    def __init__(self, app_):
        self.app = app_

    def set(self,value):
        LOG.debug('Could not set dashboard')
        pass

    def get(self):
        test = self.app.test
        if test:
            return test.dashboard()
        return None


class configMapping(object):
    def __init__(self, app):
        self.loglevel = loglevel(app)
        self.cfgname = cfgname(app)
        self.dashboard = dashboard(app)


class Application(web.Application):
    """
        Pi test bench Web server
    """
    def __init__(self, cfg_dir, web_root):
        self.cfg_list = []
        self.cfg_name = None
        self.test = None
        self.cfg = configMapping(self)
        self.cfg_dir = cfg_dir
        self._elements = {}
        LOG.debug('config list (%s)', cfg_dir)
        for root, folder, files in os.walk(cfg_dir):
            for name in files:
                splited = name.split('.')
                if len(splited) != 2 or splited[0] == '__init__' or \
                                   splited[1] != 'py':
                    continue
                self.cfg_list.append(splited[0])
                LOG.debug('    '+splited[0])

        handlers = [
            (r"/", MainHandler),
            (r"/shell", shellsocket.handler),
            (r"/state", statesocket.handler),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(web_root, "templates"),
            static_path=os.path.join(web_root, "static"),
            xsrf_cookies=False,
        )
        super().__init__(handlers, **settings)

        cache = statesocket.handler.get_save("")
        """ we cannot restore files list """
        cache.pop('files',None)

        """ load cache in right order """
        for sect in ['config', 'test']:
            sect_val = cache.pop(sect, None)

            if sect_val == None:
                continue

            for key, value in sect_val.items():
                try:
                    self.cmd(sect, key, value)
                except:
                    LOG.exception('Failed to load %s %s=%s'%(sect, key, value))

        """ load remain cache  """
        for sect, sect_val in cache.items():
            for key, value in sect_val.items():
                try:
                    self.cmd(sect, key, value)
                except:
                    LOG.exception('Failed to load %s %s=%s'%(sect, key, value))



    def cmd(self, sect, key, value):
        changed = None
        if sect == 'config':
            attr = getattr(self.cfg, key)
            if value != None:
                changed = attr.set(value)
            value = attr.get()
            statesocket.handler.set(sect, key, value)

        elif sect == 'test':
            res = None
            if self.test:
                elem = self.test.get_element(key)
                if elem != None:
                    if value != None:
                        changed = elem.set(value)
                    res = elem.get()

            statesocket.handler.set(sect, key, res)

        elif sect == 'state':
            if value != None:
                changed = getattr(statesocket.handler, 'set_'+key)(value)

        elif sect == 'files':
            if self.test:
                if value != None:
                    getattr(self.test.files, key)(value)
                """ always return files list """
                key     = 'list'
                value   = self.test.files.list()
            else:
                value = None
            statesocket.handler.set(sect, key, value)

        if changed:
            for i in changed:
                self.cmd(sect, i, None)

    def kill(self):
        try:
            statesocket.handler.set_save("")
        except:
            LOG.exception("Save Failed!")
        try:
            self.cfg.cfgname.set('---')
        except:
            LOG.exception("Stop test Failed!")

if __name__ == "__main__":

    topDir = os.path.dirname(os.path.split(os.path.abspath(__file__))[0])
    os.chdir(topDir)
    resDir = os.path.join(topDir, "resources", "html")
    cfgDir = os.path.join(topDir, 'src', 'test', 'cfg')
    sys.path.append(cfgDir)

    LOG = logging.getLogger()

    parser = argparse.ArgumentParser(description='Pi-test bench.')

    parser.add_argument('--port', type=int, default=8888,
                        help='listen port')
    parser.add_argument('--root', type=str, default=resDir,
                        help='Webserver root')
    parser.add_argument('-v', '--verbose', action="count",
                        help="verbose level... Repeat up to three times.")

    args = parser.parse_args()

    if not args.verbose:
        LOG.setLevel('ERROR')
    elif args.verbose == 1:
        LOG.setLevel('WARNING')
    elif args.verbose == 2:
        LOG.setLevel('INFO')
    elif args.verbose >= 3:
        LOG.setLevel('DEBUG')
    else:
        LOG.critical("UNEXPLAINED NEGATIVE COUNT!")

    LOG.info("resources dir : "+args.root)

    app = Application(cfgDir, args.root)
    app.listen(args.port)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        LOG.info("Ctrl-C received killing")
    finally:
        app.kill()

